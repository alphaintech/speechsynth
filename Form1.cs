﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Speech.Synthesis;



namespace TryTTS
{
    public partial class Form1 : Form
    {
        SpeechSynthesizer synth;
        System.Media.SoundPlayer player =
                new System.Media.SoundPlayer();
        public Form1()
        {
            InitializeComponent();
            synth = new SpeechSynthesizer();
            synth.SetOutputToDefaultAudioDevice();
        }

        List<String> praseList = new List<String>();
        String word = "";

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked && 
                textBox1.Text.Length > 0 &&
                    (/*textBox1.Text.Substring(textBox1.Text.Length - 1, 1).Equals(" ") ||*/
                    textBox1.Text.Substring(textBox1.Text.Length - 1, 1).Equals("\n") )
                    )
            {
                praseList.Add(textBox1.Text);
                textBox1.Text = "";
                label2.Text += praseList.Last(); 
                
                SpeakText(praseList.Last());
            }
            //synth.Speak(textBox1.Text);
            label1.Text = textBox1.Text;
        }

        private void SpeakText(string text)
        {
            synth.Speak(text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SpeakText(textBox1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Audio Files (.wav)|*.wav";


            if (dialog.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = dialog.SafeFileName;
                string path = dialog.FileName;
                PlaySound(path);
            }
        }
        
        private void PlaySound(string path)
        {
            
            player.SoundLocation = path;
            player.Load();
            player.Play();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(player.IsLoadCompleted)
                player.Play();
        }


    }
}
